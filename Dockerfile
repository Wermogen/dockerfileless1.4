FROM ubuntu:latest as build
RUN apt update && apt-get install -y wget gcc make libpcre3-dev libpcre3-dev zlib1g zlib1g-dev libssl-dev libnginx-mod-http-lua
RUN wget http://nginx.org/download/nginx-1.18.0.tar.gz && tar xvfz nginx-1.18.0.tar.gz && cd nginx-1.18.0 && ./configure && make && make install

FROM ubuntu:latest
WORKDIR /usr/local/nginx
COPY --from=build /usr/local/nginx .
CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]
RUN echo "NGINX DONE"
RUN rm -r /usr/local/nginx/conf/nginx.conf
RUN echo "Remove cfg NGINX done"
COPY ./nginx.conf /usr/local/nginx/conf/nginx.conf
RUN echo "Copy cfg NGINX done"
RUN echo "well done"
